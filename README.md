# Task manager project

Task manager. Desktop приложение. 

<img src='https://gitlab.com/rumexpert7/todo-list/-/raw/master/frontend/public/img/task_manager.jpg?ref_type=heads'> 

## Описание

**Task manager** - это приложение для планирования ежедневных задач. Оно решает проблему организации и ведения списка задач. 

- Все задачи расположены в понятной таблице
- Деление на категории срочных, средних и не срочных задач
- Удобрая фильтрация по категориям
- Быстрое изменение созданных задач
- Мнгновенное удаление выполненных задач, для чистой истории
- Интерфейс адаптирован под desktop экраны

## Версии технологий

 - php 8.2.16
 - composer 2.7.2
 - Laravel 10
 - vue 3.4 (composition api)
 - node.js 20.11 lts
 - docker 24.0.5
 - docker-compose 1.29.2


## Установка

1. Склонируйте репозиторий:

   ```bash
   git clone https://gitlab.com/rumexpert7/todo-list.git

2. Пропишите в файле host следующие домены:

   ```bash
   127.0.0.1         backend.local frontend.local

3. Создайте образы докер:

   ```bash
   docker-compose build   

4. Запустите контейнеры:

   ```bash
   docker-compose up -d

5. Установите сomposer на локальной машине и установите зависимости в папке backend:

   ```bash
   composer install

6. файл /backend/.env.expample переименовать в /backend/.env:



7. Запустите миграцию laravel в контейнере backend:

   ```bash
   docker-compose exec --user=www backend bash -c "php artisan migrate"

8. Создайте в базовые данные для демонстрации:

   ```bash
   docker-compose exec --user=www backend bash -c "php artisan db:seed"

8. Сгенерируйте секретный ключ laravel:

   ```bash
   docker-compose exec --user=www backend bash -c "php artisan key:generate"

## Приложение готово к демонстрации

   - Проект подготовлен исключительно для локальной разработки и демонстрации 
   - Проект не предназначен для размещения на production сервер.