function inputFilter (newStr, oldStr)  {

    const regExp1 = /^[a-zA-Zа-яА-Я\s]*$/ // get only letters and spaces
    const regExp2 = /^[0-9]{1,12}$/       // get only number no more 12 digits

    if(regExp1.test(oldStr) && regExp1.test(newStr))  {
        return true
        //exept first and last cases 
    } else if ((oldStr == "" ? true : regExp2.test(oldStr)) && newStr == "" ? true : regExp2.test(newStr)) {
        return true
    } else {
        return false
    }
}

export default inputFilter;