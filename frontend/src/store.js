import { createStore } from 'vuex'


const store = createStore({
    state () {
      return {
        taskList: [], //$store.state.taskList
        whichEdit: -1 //$store.state.whichEdit
      }
    },
  })


  export default store; 