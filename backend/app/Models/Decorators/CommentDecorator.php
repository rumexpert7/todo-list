<?php

namespace App\Models\Decorators;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CommentDecorator extends Model
{

    protected  Model $obj;

    public function __construct(Model $model)
    {
        $this->obj = $model;
    }

    public function getAll(): object
    {
        //execute origin operation
        $tasks = $this->obj->getAll();

        return $this->addComments($tasks);
    }

    public function createTask(array $data): object
    {
        //execute origin operation
        $task = $this->obj->createTask($data);

        return $this->addComments($task);
    }

    public function updateTask(array $data,int $id): object
    {
        //execute origin operation
        $task = $this->obj->updateTask($data, $id);

         return $this->addComments($task);
    }

    private function addComments(object $tasks): object
    {
        $updatedTasks = $tasks->map(function ($task) {
            if ($task->type_name == 'issue') {
                $task->task .= " /This is ISSUE!";
            } elseif ($task->type_name == 'future') {
                $task->task .= " /This is FUTURE!";
            }
            return $task;
        });

         return $updatedTasks;
    }
}
