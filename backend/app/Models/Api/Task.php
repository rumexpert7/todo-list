<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use App\Models\TaskType;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;

    protected $guarded = false;
    public $timestamps = false;

    static function getAll()
    {
        return DB::table('tasks')
            ->join('task_types', 'tasks.type_id', '=', 'task_types.id')
            ->select(
                'tasks.id',
                'tasks.task',
                'tasks.status',
                'task_types.name as type_name'
            )
            ->get();
    }

    static function getOne($id)
    {
        
        return DB::table('tasks')
            ->join('task_types', 'tasks.type_id', '=', 'task_types.id')
            ->select(
                'tasks.id',
                'tasks.task',
                'tasks.status',
                'task_types.name as type_name'
            )
            ->where('tasks.id', $id)
            ->get();
    }

    static function createTask($data)
    {

        $taskId = DB::table('tasks')
            ->insertGetId(
                [
                    'task' => $data['task'],
                    'status' => $data['status'],
                    'type_id' => DB::raw('(SELECT id FROM task_types WHERE name = "' . $data['type_name'] . '")'),
                ]
            );

        return Task::getOne($taskId);
    }

    static function updateTask($data, $id)
    {

        DB::table('tasks')
            ->where('id', $id)
            ->update(
                [
                    'task' => $data['task'],
                    'status' => $data['status'],
                    'type_id' => DB::raw('(SELECT id FROM task_types WHERE name = "' . $data['type_name'] . '")'),
                ]
            );
        return Task::getOne($id);
    }
}
