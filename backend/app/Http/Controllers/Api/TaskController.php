<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Api\Task;
use App\Http\Controllers\Controller;
use App\Models\Decorators\CommentDecorator;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //use decorator for add comments
        $commentMaker = new CommentDecorator(new Task);
        return $commentMaker->getAll();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTaskRequest $request)
    {
        $data = $request->validated();
        //use decorator for add comment for new task seeing correctly
        $commentMaker = new CommentDecorator(new Task);
        return $commentMaker->createTask($data);
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
            $data = $request->validated();
            //use decorator for add comment for new task seeing correctly
            $commentMaker = new CommentDecorator(new Task);
            return $commentMaker->updateTask($data, $task->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return response([]);
    }
}
