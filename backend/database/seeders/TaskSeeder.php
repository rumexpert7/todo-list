<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Api\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Task::create([
            'task' => 'Купить продуктов в Ашане',
            'status' => 'nomatter',
            'type_id' => 3,
        ]);
        Task::create([
            'task' => 'Сделать ремонт в квартире',
            'status' => 'hot',
            'type_id' => 2,
        ]);
        Task::create([
            'task' => 'Купить летнюю резину для автомобиля',
            'status' => 'medium',
            'type_id' => 3,
        ]);
        Task::create([
            'task' => 'Подстричь лужайку около дома',
            'status' => 'hot',
            'type_id' => 1,
        ]);
        Task::create([
            'task' => 'Наколоть дров для печи',
            'status' => 'nomatter',
            'type_id' => 2,
        ]);
    }
}
