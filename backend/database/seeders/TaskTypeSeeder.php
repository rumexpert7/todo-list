<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TaskType;

class TaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TaskType::create([
            'name' => 'no comment',
        ]);

        TaskType::create([
            'name' => 'issue',
        ]);

        TaskType::create([
            'name' => 'future',
        ]);

    }
}
